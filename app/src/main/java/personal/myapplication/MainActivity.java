package personal.myapplication;


import android.app.Activity;
//import android.app.Fragment;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;


import android.support.v4.app.Fragment;
//import android.support.v4.app.LoaderManager;

//import android.support.v4.content.Loader;
//import android.support.v4.content.AsyncTaskLoader;


import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.graphics.Bitmap;



import com.androidquery.AQuery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.IOException;

import personal.myapplication.R;

public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Document> {


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            Log.d("タグ","OnCreate");


            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();

        }
    }


    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Log.d("タグ","クリエイトローダー前");
        MyAsyncTaskLoader appLoader = new MyAsyncTaskLoader(getApplication());
        Log.d("タグ","クリエイトローダーあと");
        // loaderの開始
        appLoader.forceLoad();

        return appLoader;
    }

    @Override
    public void onLoadFinished(Loader loader, Document doc) {
        Log.d("タグ","ろーだー前");
        if (doc != null) {
            // 取得成功
            Log.d("タグ","エレメント");
           Element content = doc.getElementById("mp-tfa");
            Elements images = content.getElementsByTag("img");
            Element image = images.get(0);
            String imagePath = "http:" + image.attr("src");

            AQuery imageView = new AQuery(this);
            AQuery aQuery = imageView.id(R.id.imageView).visible().webImage(imagePath, true, false, 0);
            Log.d("imagePath", imagePath);
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            getLoaderManager().initLoader(0, null, this);
            Log.d("タグ","クリエイトビュー");

            return rootView;
        }
    }

    public static class MyAsyncTaskLoader extends AsyncTaskLoader<Document> {


        public MyAsyncTaskLoader(Context context) {
            super(context);
            Log.d("タグ","まいあしー");
        }

        @Override
        public Document loadInBackground() {
            Document doc = null;
            try {
                // HTML取得
                Log.d("タグ","ジェイソープ");
                doc = Jsoup.connect("http://en.wikipedia.org/wiki/Main_Page").get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return doc;
        }
    }
}